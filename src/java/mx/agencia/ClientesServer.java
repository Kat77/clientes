/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.agencia;

import dao.UsuarioDAO;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import pojo.Usuario;

/**
 * @author Cinthia 
 * @author Katya
 * @author Andrés 
 */
@WebService(serviceName = "ClientesServer")
public class ClientesServer {

    UsuarioDAO usuarioDao = new UsuarioDAO();
    /**
     * Servicio web de prueba
     * @param txt
     * @return 
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }

    /**
     * Servicio web "getAllUsuarios": 
     * Obtiene todos los usuarios registrados del sistema.
     * @return Todos los usuarios registrados del sistema.
     */
    @WebMethod(operationName = "getAllUsuarios")
    public List<Usuario> getAllUsuarios() {
        //TODO write your implementation code here:
        return usuarioDao.getAllUsuarios();
    }
    
    /**
     * Servicio web "getUsuario": 
     * Obtiene un usuario en específico del sistema.
     * @param id
     * @return Un objeto de tipo Usuario.
     */
    @WebMethod(operationName = "getUsuario")
    public Usuario getUsuario(@WebParam(name = "id") int id) {
        //TODO write your implementation code here:
        return usuarioDao.getUsuario(id);
    }

    /**
     * Servicio web "login":
     * Obtiene los datos de un usuario para que pueda ser logueado en el sistema.
     * @param username
     * @param password
     * @return Un usuario y contraseña.
     */
    @WebMethod(operationName = "login")
    public Usuario login(@WebParam(name = "username") String username, @WebParam(name = "password") String password ) {
        //TODO write your implementation code here:
        return usuarioDao.login(username, password);
    }
    
    /**
     * Servicio web "register": 
     * Obtiene los parámetros necesarios para que un usuario sea registrado en el sistema.
     * @param id
     * @param name
     * @param password
     * @param username
     * @return La operación de registro de usuario.
     */
    @WebMethod(operationName = "register")
    public int register(@WebParam(name = "id") int id, 
            @WebParam(name = "name") String name, 
            @WebParam(name = "password") String password, 
            @WebParam(name = "username") String username ) {
        //TODO write your implementation code here:
        return usuarioDao.register(id, name, password, username);
    }
    
    /**
     * Servicio web "deleteUsuario": 
     * Obtiene el id de un usuario para que sea eliminado del sistema.
     * @param id
     * @return La operación de eliminado de un usuario.
     */
    @WebMethod(operationName = "deleteUsuario")
    public int deleteUsuario(@WebParam(name = "id") int id) {
        //TODO write your implementation code here:
        return usuarioDao.deleteUsuario(id);
    }

    /**
     * Servicio web "insertUsuario": 
     * Obtiene los parámetros necesarios para que un nuevo usuario sea agregado en el sistema.
     * @param id
     * @param name
     * @param password
     * @param tipoUsuario
     * @param username
     * @return La operación de insertado de un usuario.
     */
    @WebMethod(operationName = "insertUsuario")
    public int insertUsuario(
            @WebParam(name = "id") int id, 
            @WebParam(name = "name") String name, 
            @WebParam(name = "password") String password, 
            @WebParam(name = "tipoUsuario") String tipoUsuario, 
            @WebParam(name = "username") String username) {
        //TODO write your implementation code here:
        return usuarioDao.insertUsuario(id, name, password, username, tipoUsuario);
    }

    /**
     * Servicio web "updateUsuario": 
     * Obtiene los parámetros necesarios para que los datos de un usuario del sistema sean actualizados.
     * @param id
     * @param name
     * @param password
     * @param tipoUsuario
     * @param username
     * @return La operación de actualizado de datos de un usuario.
     */
    @WebMethod(operationName = "updateUsuario")
    public int updateUsuario(
            @WebParam(name = "id") int id, 
            @WebParam(name = "name") String name, 
            @WebParam(name = "password") String password, 
            @WebParam(name = "tipoUsuario") String tipoUsuario, 
            @WebParam(name = "username") String username) {
        //TODO write your implementation code here:
        return usuarioDao.updateUsuario(id, name, password, username, tipoUsuario);
    }
}
