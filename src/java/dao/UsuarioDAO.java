/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pojo.Usuario;
import util.Conexion;

/**
 * @author Cinthia 
 * @author Katya
 * @author Andrés 
 */
public class UsuarioDAO {
    /*
    Inicialización de variables
    */
    private Conexion conn;
    ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

    public UsuarioDAO() {
        conn = new Conexion();
        //Usuario usuario= new Usuario(1, "Kat", "Kat7", "Katita", "Normal");
        //Usuario usuario1= new Usuario(2, "Lulu", "Lulu", "Lulu", "Normal");
        //Usuario usuario2= new Usuario(3, "Ana", "Ana", "Ana", "Normal");
        //Usuario usuario3= new Usuario(4, "Pedro", "Pedro", "Pedro", "Admin");
        //usuarios.add(usuario);
        //usuarios.add(usuario1);
        //usuarios.add(usuario2);
        //usuarios.add(usuario3);
    }
    
    /*
    Método que devuelve todos los usuarios del sistema.
    */
    public List<Usuario> getAllUsuarios() {
         List<Usuario> res = new ArrayList<Usuario>();
        try {
            ResultSet rs = conn.query("SELECT * FROM usuarios");
            while (rs != null && rs.next()) {
                res.add(new Usuario(rs));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;

    }

    /*
    Método que devuelve un usuario en específico
    */
    public Usuario getUsuario(int id) {
        Usuario res = null;
        try {
            ResultSet rs = conn.query("SELECT * FROM usuarios WHERE usuarios_id=" + id);
            if (rs != null && rs.next()) {
                res = new Usuario(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    /*
    Método que elimina un usuario del sistema.
    */
    public int deleteUsuario(int id) {
         int res = 0;
        conn.execute("DELETE FROM usuarios WHERE usuario_id=" + id);
        return res;

    }

    /*
    Método que inserta un usuario en el sistema.
    */
    public int insertUsuario(int id, String name, String password, String username, String tipoUsuario) {
        int res = 0;
        conn.execute("INSERT INTO usuarios(usuario_id, nombre, password, username, tipo_usuario) VALUES(" + id + ", '" + name + "', '" + password + "', '" + username + "', '" + tipoUsuario + "'");
        return res;
    }

    /*
    Método que actualizado los datos de un usuario del sistema.
    */
    public int updateUsuario(int id, String name, String password, String username, String tipoUsuario) {
        int res = 0;
        conn.execute("UPDATE usuarios SET nombre ='" + name + "', username='" + username + "', password='" + password + "', tipo_usuario='" + tipoUsuario + "' WHERE usuarios_id=" + id);
        return res;

    }
    
    /*
    Método que permite que un usuario pueda ser logueado en el sistema.
    */
    public Usuario login(String username, String password){
         Usuario res = null;
        try {
            ResultSet rs = conn.query("SELECT * FROM usuarios WHERE username='" + username + "' and password='" + password + "'");
            if (rs != null && rs.next()) {
                res = new Usuario(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    /*
    Método que permite que un usuario sea registrado en el sistema.
    */
    public int register(int id, String name, String password, String username) {
        int res = 0;
        conn.execute("INSERT INTO usuarios(usuario_id, nombre, password, username, tipo_usuario) VALUES(" + id + ", '" + name + "', '" + password + "', '" + username + "', 'Cliente'");
        return res;
        //Usuario usuario= new Usuario(id, username, password, name, "1");
        //usuarios.add(usuario);
        //return 1;
    }

    /*
    Método main de la clase UsuarioDAO
    */
    public static void main(String args[]) {
        UsuarioDAO x = new UsuarioDAO();
//        System.out.println("getAllRegion = " + x.getAllRegion());
//        System.out.println("getRegion = " + x.getRegion(1));
//        System.out.println("insertRegion = " + x.insertRegion(5, "Oceania"));
//        System.out.println("deleteRegion = " + x.deleteRegion(5));
        //System.out.println("updateRegion = " + x.updateUsuario(5, "Otra cosa"));
    }
}
