/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Cinthia 
 * @author Katya
 * @author Andrés 
 */
public class Usuario {
    
    /*
    Inicialización de variables
    */
    private int id;
    private String username;
    private String contrasena;
    private String nombre;
    private String tipoUsuario;

    public Usuario() {
    }

    /*
    Constructor de la clase Usuario
    */
    public Usuario(int id, String username, String contrasena, String nombre, String tipoUsuario) {
        this.id = id;
        this.username = username;
        this.contrasena = contrasena;
        this.nombre = nombre;
        this.tipoUsuario = tipoUsuario;
    }
    
    /*
    Constructor de la clase Usuario para la base de datos
    */
    public Usuario(ResultSet rs) {
        try {
            this.id = rs.getInt("USUARIOS_ID");
            this.nombre = rs.getString("NOMBRE");
            this.contrasena = rs.getString("PASSWORD");
            this.username = rs.getString("USERNAME");
            this.tipoUsuario = rs.getString("TIPO_USUARIO");
        } catch (SQLException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /*
    Devuelve el id del usuario
    */
    public int getId() {
        return id;
    }

    /*
    Fija el id del usuario
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    Devuelve el nombre de usuario del usuario
    */
    public String getUsername() {
        return username;
    }

    /*
    Fija el nombre de usuario del usuario
    */
    public void setUsername(String username) {
        this.username = username;
    }

    /*
    Devuelve la contraseña del usuario
    */
    public String getContrasena() {
        return contrasena;
    }

    /*
    Fija la contraseña del usuario
    */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /*
    Devuelve el nombre del usuario
    */
    public String getNombre() {
        return nombre;
    }

    /*
    Fija el nombre del usuario
    */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /*
    Devuelve el tipo de usuario del usuario
    */
    public String getTipoUsuario() {
        return tipoUsuario;
    }

    /*
    Fija el tipo de usuario del usuario
    */
    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    /*
    Método "toString"
    */
    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", username=" + username + ", contrasena=" + contrasena + ", nombre=" + nombre + ", tipoUsuario=" + tipoUsuario + '}';
    }


}